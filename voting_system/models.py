from django.db import models
from django.contrib.auth.models import User


class Participant(models.Model):
    full_name = models.CharField(max_length=100)

    def __str__(self):
        return str(self.full_name)


class Vote(models.Model):
    referee = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name='نام داور')
    participant = models.ForeignKey(Participant, on_delete=models.CASCADE, verbose_name='نام شرکت کننده')
    s1 = models.PositiveSmallIntegerField(default=0, verbose_name='وضوح و شفافیت موضوع')
    s2 = models.PositiveSmallIntegerField(default=0, verbose_name='توانایی انتقال مطالب به مخاطبین و ارتباط با آنها')
    s3 = models.PositiveSmallIntegerField(default=0, verbose_name='شیوه بیان جذاب')
    s4 = models.PositiveSmallIntegerField(default=0, verbose_name='ظاهر، عملکرد حرفه ای یا مناسب و استفاده از زبان بدن')
    s5 = models.PositiveSmallIntegerField(default=0, verbose_name='سازماندهی مناسب اسلایدها')
    s6 = models.PositiveSmallIntegerField(default=0,
                                          verbose_name='تسلط به موضوع ارائه، داشتن آرامش و اعتماد به نفس در ارائه')
    s7 = models.PositiveSmallIntegerField(default=0,
                                          verbose_name='استفاده از تصاویر به طور موثر و کیفیت اسلایدها و استفاده از محتوای بصری')
    s8 = models.PositiveSmallIntegerField(default=0, verbose_name='مدیریت زمان')
    final = models.BooleanField(default=0, verbose_name='نهایی')

    def __str__(self):
        return ' به ' + str(self.participant) + ' ' + str(self.referee) + ' ' + 'رای '


class number(models.Model):
    num = models.PositiveSmallIntegerField(default=0)

    def __str__(self):
        return 'عدد داور ناظر'
